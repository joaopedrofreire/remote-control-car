// Definindo os pinos de utilização do Driver L298.
// Pinos do motor esquerdo
const int pin2 = 2;
const int pin3 = 3;
// Pinos do motor direito
const int pin4 = 4;
const int pin5 = 5;


// Variáveis Úteis
int state_temp;
int vSpeed = 200;   // Define velocidade padrão 0 - 255.
char state;
 
void setup() {
  // Iniciando as portas como saída.
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);
  pinMode(pin5, OUTPUT);
  
  // Iniciando a comunicação serial.
  Serial.begin(9600);
}
 
void loop() {
 
  // Salva os valores da variável 'state'
  if (Serial.available() > 0) {
    state_temp = Serial.read();
    state = state_temp;
  }
 
  // Se o estado recebido for igual a '8', o carro se movimenta para frente.
  if (state == '8') {
    Serial.println("Comando para Frente");
    // Comandos para o motor esquerdo para frente
    analogWrite(pin2, vSpeed);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito para frente
    analogWrite(pin4, vSpeed);
    analogWrite(pin5, 0);
  }
    // Se o estado recebido for igual a '7', o carro se movimenta para Frente Esquerda.
    else if (state == '7') {  
    Serial.println("Comando para Frente-Esquerda");
    // Comandos para o motor esquerdo para frente
    analogWrite(pin2, 100);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito para frente
    analogWrite(pin4, vSpeed);
    analogWrite(pin5, 0);
  }
    // Se o estado recebido for igual a '9', o carro se movimenta para Frente Direita.
    else if (state == '9') {   
    Serial.println("Comando para Frente-Direita");
    // Comandos para o motor esquerdo para frente
    analogWrite(pin2, vSpeed);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito para frente
    analogWrite(pin4, 100);
    analogWrite(pin5, 0);
  }
  // Se o estado recebido for igual a '2', o carro se movimenta para trás.
  else if (state == '2') { 
    Serial.println("Comando para Trás");
    // Comandos para o motor esquerdo para trás
    analogWrite(pin2, 0);
    analogWrite(pin3, vSpeed);
    
    // Comandos para o motor direito para trás
    analogWrite(pin4, 0);
    analogWrite(pin5, vSpeed);
  }
   // Se o estado recebido for igual a '1', o carro se movimenta para Trás Esquerda.
   else if (state == '1') {  
    Serial.println("Comando para Trás-Esquerda");
    // Comandos para o motor esquerdo para trás
    analogWrite(pin2, 0);
    analogWrite(pin3, 100);
    
    // Comandos para o motor direito para trás
    analogWrite(pin4, 0);
    analogWrite(pin5, vSpeed);
  }
  // Se o estado recebido for igual a '3', o carro se movimenta para Trás Direita.
  else if (state == '3') {  
    Serial.println("Comando para Trás-Direita");
    // Comandos para o motor esquerdo para trás
    analogWrite(pin2, 0);
    analogWrite(pin3, vSpeed);
    
    // Comandos para o motor direito para trás
    analogWrite(pin4, 0);
    analogWrite(pin5, 100);
  }
  // Se o estado recebido for igual a '4', o carro se movimenta para esquerda.
  else if (state == '4') {   
    Serial.print("Comando para Esquerda");
    // Comandos para o motor esquerdo parar
    analogWrite(pin2, 0);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito para frente
    analogWrite(pin4, vSpeed);
    analogWrite(pin5, 0);
  }
  // Se o estado recebido for igual a '6', o carro se movimenta para direita.
  else if (state == '6') {   
    Serial.println("Comando para Direita");
    // Comandos para o motor esquerdo para frente
    analogWrite(pin2, vSpeed);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito parar
    analogWrite(pin4, 0);
    analogWrite(pin5, 0);
  // Se o estado recebido for igual a '5', o carro permanece parado.
  else if (state == '5') {   
    Serial.print("Comando para Parar");
    // Comandos para o motor esquerdo parar
    analogWrite(pin2, 0);
    analogWrite(pin3, 0);
    
    // Comandos para o motor direito parar
    analogWrite(pin4, 0);
    analogWrite(pin5, 0);
  }
}
